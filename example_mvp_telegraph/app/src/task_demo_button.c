/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : task_button.c
 * @date   : Jul 1, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "arch.h"
#include "sampler.h"
#include "demo.h"
#include "ui.h"
/********************** macros and definitions *******************************/

enum
{
  STATE_ENABLE, STATE_RECORDING, STATE_DISABLE,
};

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static struct
{
  int state;
  bool value;
  ui_button_sign_t sign_to_start;
  ui_button_sign_t sign_to_stop;
} self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static uint8_t sampler_read_handler_(void)
{
  return self_.value ? 1 : 0;
}

static void sign_start_cb_(bool state)
{
  static bool _state = false;
  if (true == _state && false == state)
  {
    demo_event(DEMO_EVENT_RECORD_START);
    gpio_driver_write(GPIO_DRIVER_ID_LED3, false);
  }
  else if (true == _state)
  {
    gpio_driver_write(GPIO_DRIVER_ID_LED3, true);
  }
  _state = state;
}

static void sign_stop_cb_(bool state)
{
  if (true == state)
  {
    demo_event(DEMO_EVENT_RECORD_STOP);
  }
}

static void state_enable_(void)
{
  ui_button_sign_write(&self_.sign_to_start, (self_.value) ? 1 : 0);
}

static void state_recording_(void)
{
  gpio_driver_write(GPIO_DRIVER_ID_LED3, self_.value);
  ui_button_sign_write(&self_.sign_to_stop, (self_.value) ? 1 : 0);
  sampler_tick();
  if(true == sampler_is_full())
  {
    gpio_driver_write(GPIO_DRIVER_ID_LED2, true);
  }
}

static void state_disable_(void)
{
  gpio_driver_write(GPIO_DRIVER_ID_LED2, true);
  return;
}

/********************** external functions definition ************************/

void demo_button_recording_start(uint8_t *buffer, size_t buffer_size)
{
  if(STATE_ENABLE == self_.state)
  {
    sampler_start(buffer, buffer_size);
    ui_button_sign_reset(&self_.sign_to_stop);
    self_.state = STATE_RECORDING;
  }
}

size_t demo_button_recording_stop(void)
{
  if(STATE_RECORDING == self_.state)
  {
    self_.state = STATE_DISABLE;
    return sampler_count();
  }
  return 0;
}

void demo_button_disable(void)
{
  gpio_driver_write(GPIO_DRIVER_ID_LED2, false);
  self_.state = STATE_DISABLE;
}

void demo_button_enable(void)
{
  gpio_driver_write(GPIO_DRIVER_ID_LED2, false);
  ui_button_sign_reset(&self_.sign_to_start);
  self_.state = STATE_ENABLE;
}

void task_demo_button(void *pvParameters)
{
  TickType_t xLastWakeTime;
  const TickType_t xFrequency = 20;
  xLastWakeTime = xTaskGetTickCount();

  self_.state = STATE_ENABLE;
  ui_button_sign_init(&self_.sign_to_start, 1, 2000 / 20, sign_start_cb_);
  ui_button_sign_init(&self_.sign_to_stop, 0, 3000 / 20, sign_stop_cb_);
  sampler_init(sampler_read_handler_);

  while (true)
  {
    self_.value = gpio_driver_read(GPIO_DRIVER_ID_BUTTON);
    switch (self_.state)
    {
      case STATE_ENABLE:
        state_enable_();
        break;
      case STATE_RECORDING:
        state_recording_();
        break;
      case STATE_DISABLE:
      default:
        state_disable_();
        break;
    }

    vTaskDelayUntil(&xLastWakeTime, xFrequency);
  }
}

/********************** end of file ******************************************/
