/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : task_button.c
 * @date   : Jul 1, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "arch.h"
#include "repeater.h"
#include "demo.h"
#include "ui.h"
/********************** macros and definitions *******************************/

enum
{
  STATE_ENABLE, STATE_DISABLE,
};

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static struct
{
  int state;
} self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void repeater_write_handler_(uint8_t value)
{
  gpio_driver_write(GPIO_DRIVER_ID_LED3, (1 == value) ? true : false);
}

static void state_enable_(void)
{
  repeater_tick();
  if(true == repeater_is_empty())
  {
    demo_event(DEMO_EVENT_PLAY_END);
    self_.state = STATE_DISABLE;
  }
//  gpio_driver_write(GPIO_DRIVER_ID_LED1, true);
}

static void state_disable_(void)
{
//  gpio_driver_write(GPIO_DRIVER_ID_LED1, false);
  return;
}

/********************** external functions definition ************************/

void demo_repeater_start(uint8_t *buffer, size_t buffer_size)
{
  repeater_start(buffer, buffer_size);
  self_.state = STATE_ENABLE;
}

void task_demo_repeater(void *pvParameters)
{
  TickType_t xLastWakeTime;
  const TickType_t xFrequency = 20;
  xLastWakeTime = xTaskGetTickCount();

  self_.state = STATE_DISABLE;
  repeater_init(repeater_write_handler_);

  while (true)
  {
    switch (self_.state)
    {
      case STATE_ENABLE:
        state_enable_();
        break;
      case STATE_DISABLE:
      default:
        state_disable_();
        break;
    }

    vTaskDelayUntil(&xLastWakeTime, xFrequency);
  }
}

/********************** end of file ******************************************/
