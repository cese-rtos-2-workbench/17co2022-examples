/*
 * Copyright (c) YEAR NOMBRE <MAIL>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : demo.c
 * @date   : Jul 1, 2022
 * @author : NOMBRE <MAIL>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "demo.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

typedef bool (*state_event_handler_t)(demo_event_t event);

/********************** internal functions declaration ***********************/

static bool event_when_standby_(demo_event_t event);

static bool event_when_record_(demo_event_t event);

static bool event_when_play_(demo_event_t event);

/********************** internal data definition *****************************/

static struct
{
    bool leds[DEMO_LED__N];
    demo_action_handler_t action_handler;
    state_event_handler_t state_event;
    bool button;
} self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static bool event_when_standby_(demo_event_t event)
{
  switch (event)
  {
    case DEMO_EVENT_RECORD_START:
      self_.action_handler(DEMO_ACTION_RECORD_START);
      self_.state_event = event_when_record_;
      return true;
      break;
    default:
      break;
  }
  return false;
}

static bool event_when_record_(demo_event_t event)
{
  switch (event)
  {
    case DEMO_EVENT_RECORD_STOP:
      self_.action_handler(DEMO_ACTION_RECORD_STOP);
      self_.action_handler(DEMO_ACTION_PLAY_START);
      self_.state_event = event_when_play_;
      return true;
      break;
    default:
      break;
  }
  return false;
}

static bool event_when_play_(demo_event_t event)
{
  switch (event)
  {
    case DEMO_EVENT_PLAY_END:
      self_.action_handler(DEMO_ACTION_PLAY_STOP);
      self_.state_event = event_when_standby_;
      return true;
      break;
    default:
      break;
  }
  return false;
}

/********************** external functions definition ************************/

void demo_init(demo_action_handler_t action_handler)
{
  self_.action_handler = action_handler;
  for(int i = 0; i < DEMO_LED__N; ++i)
  {
    self_.leds[i] = false;
  }
  self_.leds[DEMO_LED_GREEN] = true;
  self_.action_handler(DEMO_ACTION_LED);
  self_.state_event = event_when_standby_;
}

bool demo_led(demo_led_t led)
{
  return self_.leds[led];
}

bool demo_event(demo_event_t event)
{
  return self_.state_event(event);
}

/********************** end of file ******************************************/
