/*
 * Copyright (c) YEAR NOMBRE <MAIL>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file   : demo.h
 * @date   : Jul 1, 2022
 * @author : NOMBRE <MAIL>
 * @version	v1.0.0
 */

#ifndef INC_DEMO_H_
#define INC_DEMO_H_

/********************** CPP guard ********************************************/
#ifdef __cplusplus
extern "C" {
#endif

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/********************** macros ***********************************************/

/********************** typedef **********************************************/

typedef enum
{
  DEMO_ACTION_NONE,
  DEMO_ACTION_LED,
  DEMO_ACTION_RECORD_START,
  DEMO_ACTION_RECORD_STOP,
  DEMO_ACTION_PLAY_START,
  DEMO_ACTION_PLAY_STOP,
  DEMO_ACTION__N,
} demo_action_t;

typedef enum
{
  DEMO_EVENT_RECORD_START,
  DEMO_EVENT_RECORD_STOP,
  DEMO_EVENT_PLAY_END,
  DEMO_EVENT__N,
} demo_event_t;

typedef enum
{
  DEMO_LED_RED,
  DEMO_LED_GREEN,
  DEMO_LED__N,
} demo_led_t;

typedef void (*demo_action_handler_t)(demo_action_t event);

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void demo_init(demo_action_handler_t action_handler);

bool demo_led(demo_led_t led);

bool demo_event(demo_event_t event);

/********************** End of CPP guard *************************************/
#ifdef __cplusplus
}
#endif

#endif /* INC_DEMO_H_ */
/********************** end of file ******************************************/

