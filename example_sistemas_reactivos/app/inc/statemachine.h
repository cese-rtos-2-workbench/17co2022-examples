/*
 * Copyright (c) YEAR NOMBRE <MAIL>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INC_STATEMACHINE_H_
#define INC_STATEMACHINE_H_

/********************** CPP guard ********************************************/
#ifdef __cplusplus
extern "C" {
#endif

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/********************** macros ***********************************************/

#define STATEMACHINE_TRANSITION(handle, id)\
    statemachine_transition((handle), (id));\
    return;

/********************** typedef **********************************************/

typedef struct statemachine_s statemachine_t;

typedef struct statemachine_state_s statemachine_state_t;

typedef enum
{
  STATEMACHINE_EVENT_ENTRY,
  STATEMACHINE_EVENT_ACTION,
  STATEMACHINE_EVENT_EXIT,
  STATEMACHINE_EVENT__COUNT,
} statemachine_event_t;

typedef struct statemachine_handle_s statemachine_handle_t;

typedef void (*statemachine_state_handler_t)(const  statemachine_handle_t *handle);

struct statemachine_state_s
{
    uint32_t id;
    statemachine_state_handler_t entry_handler;
    statemachine_state_handler_t action_handler;
    statemachine_state_handler_t exit_handler;
    const char *label;
};

struct statemachine_handle_s
{
    statemachine_t *statemachine;
    statemachine_state_t *state;
    void *param;
};

struct statemachine_s
{
    statemachine_state_t *states;
    size_t states_count;
    statemachine_state_t *current_state;
    statemachine_state_t *next_state;
    statemachine_state_t *previous_state;
    statemachine_handle_t handle;
    const char *label;
};

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void statemachine_init(statemachine_t *self, statemachine_state_t *states, size_t states_count, const char *label, void *param);

void statemachine_state_init(
    statemachine_t *self,
    uint32_t state_id,
    statemachine_state_handler_t entry_handler,
    statemachine_state_handler_t action_handler,
    statemachine_state_handler_t exit_handler,
    const char *label);

void statemachine_process(statemachine_t *self);

void statemachine_transition(const statemachine_handle_t *handle, uint32_t state_id);

/********************** End of CPP guard *************************************/
#ifdef __cplusplus
}
#endif

#endif /* INC_STATEMACHINE_H_ */
/********************** end of file ******************************************/

