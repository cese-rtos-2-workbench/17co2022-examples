/*
 * Copyright (c) YEAR NOMBRE <MAIL>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "statemachine.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

//static void state_handler_(statemachine_t *self, statemachine_event_t event)
//{
//  statemachine_handle_t handle;
//  handle.statemachine = self;
//  handle.state = self->current_state;
//  self->current_state->handler(&handle, event);
//}

/********************** external functions definition ************************/

void statemachine_init(statemachine_t *self, statemachine_state_t *states, size_t states_count, const char *label, void *param)
{
  self->states = states;
  self->states_count = states_count;
  self->current_state = states;
  self->next_state = states;
  self->previous_state = NULL;
  self->label = label;
  self->handle.statemachine = self;
  self->handle.state = NULL;
  self->handle.param = param;
}

void statemachine_state_init(
    statemachine_t *self,
    uint32_t state_id,
    statemachine_state_handler_t entry_handler,
    statemachine_state_handler_t action_handler,
    statemachine_state_handler_t exit_handler,
    const char *label)
{
  statemachine_state_t *state = self->states + state_id;
  state->id = state_id;
  state->entry_handler = entry_handler;
  state->action_handler = action_handler;
  state->exit_handler = exit_handler;
  state->label = label;
}

void statemachine_process(statemachine_t *self)
{
  statemachine_handle_t *handle = &self->handle;
  handle->state =self->current_state;

  if (self->previous_state != self->current_state)
  {
    if(NULL != handle->state->entry_handler)
    {
      handle->state->entry_handler(handle);
    }
  }
  self->previous_state = self->current_state;

  if(NULL != handle->state->action_handler)
  {
    handle->state->action_handler(handle);
  }

  if (self->current_state != self->next_state)
  {
    if(NULL != handle->state->exit_handler)
    {
      handle->state->exit_handler(handle);
    }
  }
  self->current_state = self->next_state;
}

void statemachine_transition(const statemachine_handle_t *handle, uint32_t state_id)
{
  handle->statemachine->next_state = handle->statemachine->states + state_id;
}

/********************** end of file ******************************************/
