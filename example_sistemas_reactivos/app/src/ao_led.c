/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "arch.h"
#include "resources.h"
#include "statemachine.h"
#include "task_led.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

enum
{
  STATE_OFF, STATE_GREEN, STATE_RED, STATE_BLUE, STATE__COUNT,
};

//typedef enum
//{
//  LED_EVENT_NONE, LED_EVENT_PULSE, LED_EVENT__COUNT,
//} led_event_t;

typedef struct
{
  led_event_t event;
} ao_led_t;

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static ao_led_t ao_led;
static statemachine_t statemachine_leds_instance;
static statemachine_t *statemachine_leds = &statemachine_leds_instance;
static statemachine_state_t statemachine_leds_states[STATE__COUNT];

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static bool event_on_pulse_(ao_led_t *ao)
{
  return (LED_EVENT_PULSE == ao->event);
}

static void event_process_(ao_led_t *ao)
{
  ao->event = LED_EVENT_NONE;
  event_queue_pop(&(ao->event));
}

static void state_off_entry_handler_(const statemachine_handle_t *handle)
{
  led_off(LED_ALL);
}

static void state_off_action_handler_(const statemachine_handle_t *handle)
{
  ao_led_t *ao_led = (ao_led_t*)handle->param;
  if (event_on_pulse_(ao_led))
  {
    statemachine_transition(handle, STATE_GREEN);
  }
}

static void state_off_exit_handler_(const statemachine_handle_t *handle)
{
  return;
}

static void state_green_entry_handler_(const statemachine_handle_t *handle)
{
  led_off(LED_ALL);
  led_on(LED_GREEN);
}

static void state_green_action_handler_(const statemachine_handle_t *handle)
{
  ao_led_t *ao_led = (ao_led_t*)handle->param;
  if (event_on_pulse_(ao_led))
  {
    statemachine_transition(handle, STATE_RED);
  }
}

static void state_green_exit_handler_(const statemachine_handle_t *handle)
{
  return;
}

static void state_red_entry_handler_(const statemachine_handle_t *handle)
{
  led_off(LED_ALL);
  led_on(LED_RED);
}

static void state_red_action_handler_(const statemachine_handle_t *handle)
{
  ao_led_t *ao_led = (ao_led_t*)handle->param;
  if (event_on_pulse_(ao_led))
  {
    statemachine_transition(handle, STATE_BLUE);
  }
}

static void state_red_exit_handler_(const statemachine_handle_t *handle)
{
  led_off(LED_ALL);
}

static void state_blue_entry_handler_(const statemachine_handle_t *handle)
{
  led_off(LED_ALL);
  led_on(LED_BLUE);
}

static void state_blue_action_handler_(const statemachine_handle_t *handle)
{
  ao_led_t *ao_led = (ao_led_t*)handle->param;
  if (event_on_pulse_(ao_led))
  {
    statemachine_transition(handle, STATE_OFF);
  }
}

static void state_blue_exit_handler_(const statemachine_handle_t *handle)
{
  return;
}

/********************** external functions definition ************************/

void ao_led_init(void)
{
  statemachine_init(statemachine_leds, statemachine_leds_states, STATE__COUNT, "LEDs", (void*)&ao_led);

  statemachine_state_init(statemachine_leds, STATE_OFF, state_off_entry_handler_, state_off_action_handler_,
                          state_off_exit_handler_, "OFF");

  statemachine_state_init(statemachine_leds, STATE_GREEN, state_green_entry_handler_, state_green_action_handler_,
                          state_green_exit_handler_, "Green");

  statemachine_state_init(statemachine_leds, STATE_RED, state_red_entry_handler_, state_red_action_handler_,
                          state_red_exit_handler_, "Red");

  statemachine_state_init(statemachine_leds, STATE_BLUE, state_blue_entry_handler_, state_blue_action_handler_,
                          state_blue_exit_handler_, "Blue");
}

void ao_led_process(void)
{
  event_process_(&ao_led);
  statemachine_process(statemachine_leds);
}

/********************** end of file ******************************************/
