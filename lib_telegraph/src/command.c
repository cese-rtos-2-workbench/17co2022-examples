/*
 * Copyright (c) YEAR NOMBRE <MAIL>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : commands.c
 * @date   : Jul 7, 2022
 * @author : NOMBRE <MAIL>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "tinysh.h"
#include "command.h"

/********************** macros and definitions *******************************/

#define UNUSED_(x)      (void)(x)
#define __weak          __attribute__((weak))

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

static void command_start_handler_(int argc, char **argv);

static void command_stop_handler_(int argc, char **argv);

static void command_save_handler_(int argc, char **argv);

static void command_play_handler_(int argc, char **argv);

static void command_delete_handler_(int argc, char **argv);

static void command_list_handler_(int argc, char **argv);

static void command_size_handler_(int argc, char **argv);

/********************** internal data definition *****************************/

tinysh_cmd_t cmd_start  = {0, "start",  "Comenzar grabación",   "D | D [int]: delay en seg | $ save D",    command_start_handler_, 0, 0, 0};
tinysh_cmd_t cmd_stop   = {0, "stop",   "Detener grabación",    "D | D [int]: delay en seg | $ stop D",    command_stop_handler_, 0, 0, 0};
tinysh_cmd_t cmd_save   = {0, "save",   "Guardar grabación",    "- |-| $ save",                            command_save_handler_, 0, 0, 0};
tinysh_cmd_t cmd_play   = {0, "play",   "Reproducir grabación", "I | I [int]: ID de mensaje | $ play I",   command_play_handler_, 0, 0, 0};
tinysh_cmd_t cmd_delete = {0, "delete", "Borrar grabación",     "I | I [int]: ID de mensaje | $ delete I", command_delete_handler_, 0, 0, 0};
tinysh_cmd_t cmd_list   = {0, "list",   "Listar mensajes",      "- |-| $ list",                            command_list_handler_, 0, 0, 0};
tinysh_cmd_t cmd_size   = {0, "size",   "Cantidad de mensajes", "- |-| $ size",                            command_size_handler_, 0, 0, 0};

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void string_out_(const char *str)
{
  size_t len = strlen(str);
  for(size_t i = 0; i < len; ++i)
  {
    command_char_out(str[i]);
  }
}

static void command_start_handler_(int argc, char **argv)
{
  if(argc < 2)
  {
    string_out_("Error");
    return;
  }
  int delay = tinysh_get_arg_int(argc, (const char **)argv, 1);
  command_start_handler(delay);
}

static void command_stop_handler_(int argc, char **argv)
{
  if(argc < 2)
  {
    string_out_("Error");
    return;
  }
  int delay = tinysh_get_arg_int(argc, (const char **)argv, 1);
  command_stop_handler(delay);
}

static void command_save_handler_(int argc, char **argv)
{
  if(argc < 1)
  {
    string_out_("Error");
    return;
  }
  command_save_handler();
}

static void command_play_handler_(int argc, char **argv)
{
  if(argc < 2)
  {
    string_out_("Error");
    return;
  }
  int id = tinysh_get_arg_int(argc, (const char **)argv, 1);
  command_play_handler(id);
}

static void command_delete_handler_(int argc, char **argv)
{
  if(argc < 2)
  {
    string_out_("Error");
    return;
  }
  int id = tinysh_get_arg_int(argc, (const char **)argv, 1);
  command_delete_handler(id);
}

static void command_list_handler_(int argc, char **argv)
{
  if(argc < 1)
  {
    string_out_("Error");
    return;
  }
  command_list_handler();
}

static void command_size_handler_(int argc, char **argv)
{
  if(argc < 1)
  {
    string_out_("Error");
    return;
  }
  command_size_handler();
}

/********************** external functions definition ************************/

void command_init(void)
{
  tinysh_init();
  tinysh_add_command(&cmd_start);
  tinysh_add_command(&cmd_stop);
  tinysh_add_command(&cmd_save);
  tinysh_add_command(&cmd_play);
  tinysh_add_command(&cmd_delete);
  tinysh_add_command(&cmd_list);
  tinysh_add_command(&cmd_size);
}

void command_deinit(void)
{
  tinysh_finish();
}

void command_char_in(char ch)
{
  tinysh_char_in(ch);
}

__weak void command_char_out(char ch)
{
  UNUSED_(ch);
}

void tinysh_char_out(unsigned char ch)
{
  command_char_out(ch);
}

__weak void command_start_handler(unsigned int delay)
{
  UNUSED_(delay);
  string_out_("Error");
  return;
}

__weak void command_stop_handler(unsigned int delay)
{
  UNUSED_(delay);
  string_out_("Error");
  return;
}

__weak void command_save_handler(void)
{
  string_out_("Error");
  return;
}

__weak void command_play_handler(unsigned int id)
{
  UNUSED_(id);
  string_out_("Error");
  return;
}

__weak void command_delete_handler(unsigned int id)
{
  UNUSED_(id);
  string_out_("Error");
  return;
}

__weak void command_list_handler(void)
{
  string_out_("Error");
  return;
}

__weak void command_size_handler(void)
{
  string_out_("Error");
  return;
}

/********************** end of file ******************************************/
