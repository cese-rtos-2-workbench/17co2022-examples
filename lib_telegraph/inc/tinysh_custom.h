#ifndef _TINYSH_CUSTOM_
#define _TINYSH_CUSTOM_

#ifdef __cplusplus
extern "C" {
#endif

//typedef void (*putchar_handler_t)(unsigned char c);
//void tinysh_set_putchar(putchar_handler_t handler);
void tinysh_init();
void tinysh_finish();

#ifdef __cplusplus
}
#endif

#endif /* _TINYSH_CUSTOM_ */
