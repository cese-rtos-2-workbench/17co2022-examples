/*
 * Copyright (c) 2022 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "utils.h"
#include "qmpool.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "supporting_Functions.h"

/********************** macros and definitions *******************************/

#define LED_MSG_MAX_LEN_                (8)

#define MEMORY_POOL_PACKET_SIZE         ((LED_MSG_MAX_LEN_ + 1) * sizeof(char))
#define MEMORY_POOL_TOTAL_BLOCKS        (5)
#define MEMORY_POOL_SIZE                (MEMORY_POOL_TOTAL_BLOCKS * MEMORY_POOL_PACKET_SIZE)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static xQueueHandle queue_h_;
static char const * const str_led_on_ = "LED ON";
static QMPool memory_pool_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void task_sender_a_(void *p_parameters)
{
  led_init();

  while (true)
  {
    led_toglle();

    if(true == led_read())
    {
      vPrintString("[a] Led ON\r\n");

      size_t str_msg_len = strlen(str_led_on_);
//      char *str_msg = (char*)pvPortMalloc(str_msg_len + 1);
      char *str_msg = (char*)QMPool_get(&memory_pool_, 0);

      if(NULL != str_msg)
      {
        strncpy(str_msg, str_led_on_, str_msg_len + 1);

        portBASE_TYPE status;
        status = xQueueSendToBack(queue_h_, (void*)&str_msg, 0);
        if (pdPASS == status)
        {
          EX_DEBUG_CONSOLE("[a] Send: %s\r\n", str_msg);
        }
      }
    }

    vTaskDelay(1000 / portTICK_RATE_MS);
  }
}

static void task_receiver_c_(void *p_parameters)
{
  portBASE_TYPE status;
  char const *str_msg = NULL;

  while (true)
  {
    status = xQueueReceive(queue_h_, (void*)&str_msg, portMAX_DELAY);
    if (pdPASS == status)
    {
      vPrintString("[c] Receive:\r\n");
      vPrintString(str_msg);
//      vPortFree((void*)str_msg);
      QMPool_put(&memory_pool_, str_msg);
      str_msg = NULL;
    }
  }
}

/********************** external functions definition ************************/

int app_init(void)
{
  void* memory_pool_storage = pvPortMalloc(MEMORY_POOL_SIZE);
  QMPool_init(&memory_pool_, memory_pool_storage, MEMORY_POOL_SIZE, MEMORY_POOL_PACKET_SIZE);

  queue_h_ = xQueueCreate(5, sizeof(int32_t));
  while (NULL == queue_h_)
  {
    // problem !
  }

  BaseType_t state;

  state = xTaskCreate(task_sender_a_, "task_sender_a", 200, NULL, 2, NULL);
  while (pdPASS != state)
  {
    // problem !
  }

  state = xTaskCreate(task_receiver_c_, "task_receiver_c", 200, NULL, 1, NULL);
  while (pdPASS != state)
  {
    // problem !
  }

//  vTaskStartScheduler();
//
//  while (true)
//  {
//    // problem !
//  }

  return 1;
}

/********************** end of file ******************************************/
