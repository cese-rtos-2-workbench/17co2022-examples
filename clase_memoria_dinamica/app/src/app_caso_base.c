/*
 * Copyright (c) 2022 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "utils.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "supporting_Functions.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void print_ptr_(void *ptr)
{
  char str[16];
  vPrintString("ptr: \r\n");
  sprintf(str, "%p", ptr);
  vPrintString(str);
  vPrintString("\r\n");
}

static void task_(void *p_parameters)
{
  while (true)
  {
    vPrintString("1\r\n");
    {
      void *ptr;
      ptr = (char*)pvPortMalloc(100);
      print_ptr_(ptr);
      vPortFree((void*)ptr);
    }

    vPrintString("2\r\n");
    {
      void *ptr;
      ptr = (char*)pvPortMalloc(100);
      print_ptr_(ptr);
      ptr = (char*)pvPortMalloc(100);
      print_ptr_(ptr);
      vPortFree((void*)ptr);
    }

    vPrintString("3\r\n");
    {
      void *ptr;
      ptr = (char*)pvPortMalloc(100);
      print_ptr_(ptr);
      vPortFree((void*)ptr);
    }

    vPrintString("4\r\n");
    {
      void *ptr;
      ptr = (char*)pvPortMalloc(16*1024);
      print_ptr_(ptr);
      vPortFree((void*)ptr);
    }

    vPrintString("5\r\n");
    {
      while(NULL != pvPortMalloc(200));
    }

    vTaskDelay(1000 / portTICK_RATE_MS);
  }
}

/********************** external functions definition ************************/

int app_init(void)
{
  BaseType_t state;

  state = xTaskCreate(task_, "task", 200, NULL, 2, NULL);
  while (pdPASS != state)
  {
    // problem !
  }

  return 1;
}

/********************** end of file ******************************************/
