/*
 * Copyright (c) 2022 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "utils.h"
#include "msgbuffer.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "supporting_Functions.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static char const * const str_led_on_ = "LED ON";

static msgbuffer_static_t msgbuffer_a_to_c_;
static msgbuffer_static_storage_element_t msgbuffer_a_to_c_buffer_[5];

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void a_led_message_routine(bool enable)
{
  if(false == enable)
  {
    return;
  }

  vPrintString("[a] Led ON\r\n");
  size_t str_msg_len = strlen(str_led_on_);

  /*
   * CRITICAL ZONE ENTRY
   */
  {
    void *p_message = msgbuffer_static_sender_message_create(&msgbuffer_a_to_c_, str_msg_len + 1);
    if(NULL != p_message)
    {
      strncpy((char*)p_message, str_led_on_, str_msg_len + 1);
    }
    msgbuffer_static_sender_post(&msgbuffer_a_to_c_, p_message, 0);
  }
  /*
   * CRITICAL ZONE EXIT
   */
}

static void task_sender_a_(void *p_parameters)
{
  led_init();

  while (true)
  {
    led_toglle();
    a_led_message_routine(led_read());
    vTaskDelay(1000 / portTICK_RATE_MS);
  }
}

static void task_receiver_c_(void *p_parameters)
{
  while (true)
  {
    /*
     * CRITICAL ZONE ENTRY
     */
    {
      void *p_message = msgbuffer_static_receiver_get(&msgbuffer_a_to_c_, portMAX_DELAY);
      if(NULL != p_message)
      {
        vPrintString("[c] msg: \r\n");
        vPrintString(p_message);
      }
      msgbuffer_static_receiver_message_destroid(&msgbuffer_a_to_c_, p_message);
    }
    /*
     * CRITICAL ZONE EXIT
     */
  }
}

/********************** external functions definition ************************/

int app_init(void)
{
  bool msgbuffer_status;
  msgbuffer_status = msgbuffer_static_init(&msgbuffer_a_to_c_, 5, msgbuffer_a_to_c_buffer_);
  while(!msgbuffer_status)
  {
    // problem !
  }

  {
    static StaticTask_t a_task_buffer_;
    static StackType_t a_task_stack_[1024];
    TaskHandle_t h_task;
    h_task = xTaskCreateStatic(task_sender_a_, "task_sender_a", 1024, NULL, 2,
                               a_task_stack_,
                               &a_task_buffer_);
    while (NULL == h_task)
    {
      // problem !
    }
  }

  {
    static StaticTask_t c_task_buffer_;
    static StackType_t c_task_stack_[1024];
    TaskHandle_t h_task;
    h_task = xTaskCreateStatic(task_receiver_c_, "task_receiver_d_", 1024, NULL, 2, c_task_stack_, &c_task_buffer_);
    while (NULL == h_task)
    {
      // problem !
    }
  }

//  vTaskStartScheduler();
//
//  while (true)
//  {
//    // problem !
//  }

  return 1;
}

/********************** end of file ******************************************/
