/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <arch.h>
#include <config.h>

#include "demo.h"
#include "task_blinking.h"
#include "task_button.h"

/********************** macros and definitions *******************************/

#define BUFFER_SIZE_                            (1000/20 * 10)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

void task_demo_button(void *pvParameters);

void demo_button_recording_start(uint8_t *buffer, size_t buffer_size);

size_t demo_button_recording_stop(void);

void demo_button_disable(void);

void demo_button_enable(void);

void task_demo_repeater(void *pvParameters);

void demo_repeater_start(uint8_t *buffer, size_t buffer_size);

/********************** internal data definition *****************************/

static uint8_t buffer_[BUFFER_SIZE_];

static size_t buffer_len_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void demo_action_handler_(demo_action_t action)
{
  switch (action)
  {
    case DEMO_ACTION_LED:
      gpio_driver_write(GPIO_DRIVER_ID_LED1, demo_led(DEMO_LED_GREEN));
      break;
    case DEMO_ACTION_RECORD_START:
      buffer_len_ = 0;
      demo_button_recording_start(buffer_, BUFFER_SIZE_);
      break;
    case DEMO_ACTION_RECORD_STOP:
      buffer_len_ = demo_button_recording_stop();
      demo_button_recording_start(buffer_, BUFFER_SIZE_);
      break;
    case DEMO_ACTION_PLAY_START:
      demo_repeater_start(buffer_, buffer_len_);
      break;
    case DEMO_ACTION_PLAY_STOP:
      demo_button_enable();
      break;
    case DEMO_ACTION_NONE:
    default:
      break;
  }
}

/********************** external functions definition ************************/

void app_init(void)
{
  gpio_driver_init();

  demo_init(demo_action_handler_);

  {
    BaseType_t status;
    status = xTaskCreate(task_demo_button, "task_demo_button", 200, NULL, tskIDLE_PRIORITY, NULL);
    while(pdPASS != status)
    {
      // error
    }
  }

  {
    BaseType_t status;
    status = xTaskCreate(task_demo_repeater, "task_demo_repeater", 200, NULL, tskIDLE_PRIORITY, NULL);
    while(pdPASS != status)
    {
      // error
    }
  }

#if 0 // mode hardware test
  {
    BaseType_t status;
    status = xTaskCreate(task_blinking, "task_button", 200, NULL, tskIDLE_PRIORITY, NULL);
    while(pdPASS != status)
    {
      // error
    }
  }

  {
    BaseType_t status;
    status = xTaskCreate(task_button, "task_button", 200, NULL, tskIDLE_PRIORITY, NULL);
    while(pdPASS != status)
    {
      // error
    }
  }
#endif
}

/********************** end of file ******************************************/
