/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : routine_example_command.c
 * @date   : May 18, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <arch.h>
#include <config.h>
#include <command.h>

/********************** macros and definitions *******************************/

#define PERIOD_ticks_                   (20 / APP_CONFIG_PERIOD_ms)
#define DELAY_ticks_                    (500 / APP_CONFIG_PERIOD_ms)

#define BUFFER_SIZE_                    (64)
/*
 * 115200 / (10 * 1000) = 11.52 bytes/ms = aprox 16 = 2^4
 * 64 / 16 = 4
 */
#define TIMEOUT_RX_MS_                 (100)
#define TIMEOUT_TX_MS_(len)            ((len) * 20)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static uint8_t buffer_[BUFFER_SIZE_];

/********************** external data definition *****************************/

extern UART_HandleTypeDef huart3;

/********************** internal functions definition ************************/

static void uart_string_write_(const char *str)
{
  size_t len = strlen(str);

  HAL_StatusTypeDef tx_status;
  tx_status = HAL_UART_Transmit(&huart3, (uint8_t*)str, len, TIMEOUT_TX_MS_(len));

  if (HAL_OK != tx_status)
  {
    // error
  }
}

static void init_(void *parameters)
{
  // clean UART
  command_init();
  HAL_UART_Receive(&huart3, buffer_, BUFFER_SIZE_, TIMEOUT_RX_MS_);
}

static void loop_(void *parameters)
{
  uint16_t rx_len;

  // RX
  {
    HAL_StatusTypeDef rx_status;
    rx_status = HAL_UARTEx_ReceiveToIdle(&huart3, buffer_, BUFFER_SIZE_, &rx_len, TIMEOUT_RX_MS_);
    if (HAL_OK != rx_status)
    {
      // error
    }
    else
    {
      for (uint16_t i = 0; i < rx_len; ++i)
      {
        command_char_in(buffer_[i]);
      }
    }
  }
}

/********************** external functions definition ************************/

void command_start_handler(unsigned int delay)
{
  uart_string_write_("command_start_handler\r");

  char str[32];
  sprintf(str, "delay: %d\r", delay);
  uart_string_write_(str);
}

void command_stop_handler(unsigned int delay)
{
  uart_string_write_("command_stop_handler\r");

  char str[32];
  sprintf(str, "delay: %d\r", delay);
  uart_string_write_(str);
}

void command_save_handler(void)
{
  uart_string_write_("command_save_handler\r");
}

void command_play_handler(unsigned int id)
{
  uart_string_write_("command_play_handler\r");

  char str[32];
  sprintf(str, "id: %d\r", id);
  uart_string_write_(str);
}

void command_delete_handler(unsigned int id)
{
  uart_string_write_("command_delete_handler\r");

  char str[32];
  sprintf(str, "id: %d\r", id);
  uart_string_write_(str);
}

void command_list_handler(void)
{
  uart_string_write_("command_list_handler\r");
}

void command_size_handler(void)
{
  uart_string_write_("command_size_handler\r");
}

void command_char_out(char ch)
{
  HAL_StatusTypeDef tx_status;
  tx_status = HAL_UART_Transmit(&huart3, (uint8_t*)&ch, 1, TIMEOUT_TX_MS_(1));

  if (HAL_OK != tx_status)
  {
    // error
    uart_string_write_("Error");
  }
}

void routine_example_polling(void *parameters)
{
  static bool _init = true;
  static uint32_t ticks = DELAY_ticks_;
  if (_init)
  {
    _init = false;
    init_(parameters);
  }

  if (0 == ticks)
  {
    ticks = (0 < PERIOD_ticks_) ? PERIOD_ticks_ : 1;
    loop_(parameters);
  }
  ticks--;
}

/********************** end of file ******************************************/
